package com.wip.core

import com.wip.core.db.PersistentLocalSourceProvider
import com.wip.core.networking.NetworkManager
import com.wip.core.repository.LocationRemoteSource
import com.wip.core.repository.LocationRepository
import com.wip.core.service.LocationService
import com.wip.core.usecase.AddLocationsUseCase
import com.wip.core.usecase.CreateLocationUseCase
import com.wip.core.usecase.GetLocationsUseCase
import com.wip.core.usecase.UpdateLocationUseCase
import org.koin.core.scope.Scope
import org.koin.dsl.module

fun coreModule(
    localSource: Scope.() -> PersistentLocalSourceProvider
) = module {
    factory { NetworkManager.create(LocationService::class.java) }

    factory { GetLocationsUseCase(get()) }
    factory { AddLocationsUseCase(get()) }
    factory { CreateLocationUseCase(get()) }
    factory { UpdateLocationUseCase(get()) }

    single { LocationRepository(get(), get()) }

    factory { LocationRemoteSource(get()) }

    factory { localSource() }
    factory { get<PersistentLocalSourceProvider>().locationPersistentLocalSourceProvider }
}