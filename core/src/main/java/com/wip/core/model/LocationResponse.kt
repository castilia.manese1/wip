package com.wip.core.model

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class LocationItemResponse(
    @Json(name = "lat")
    val lat: Double?,
    @Json(name = "latitude")
    val latitude: Double?,

    @Json(name = "lng")
    val lng: Double?,
    @Json(name = "longitude")
    val longitude: Double?,

    @Json(name = "label")
    val label: String,
    @Json(name = "address")
    val address: String,
    @Json(name = "image")
    val image: String?
)

@JsonClass(generateAdapter = true)
data class LocationResponse(
    @Json(name = "status")
    val status: String,
    @Json(name = "locations")
    val locations: List<LocationItemResponse>
)
