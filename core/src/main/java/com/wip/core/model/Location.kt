package com.wip.core.model

data class Location(
    val id: String?,
    val lat: Double?,
    val lng: Double?,
    val label: String,
    val address: String,
    val image: String?
)
