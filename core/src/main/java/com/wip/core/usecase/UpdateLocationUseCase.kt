package com.wip.core.usecase

import com.wip.core.model.Location
import com.wip.core.repository.LocationRepository

class UpdateLocationUseCase(private val repository: LocationRepository) {

    suspend operator fun invoke(location: Location, onSuccess: () -> Unit, onError: () -> Unit) {
        try {
            repository.updateLocation(location)
        } catch (e: Exception) {
            onError()
            return
        }
        onSuccess()
    }
}
