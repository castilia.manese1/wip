package com.wip.core.usecase

import com.wip.core.model.Location
import com.wip.core.repository.LocationRepository

class AddLocationsUseCase(private val repository: LocationRepository) {

    suspend operator fun invoke(locations: List<Location>, onSuccess: () -> Unit, onError: () -> Unit) {
        val result = try {
            repository.addLocationsToDb(locations)
        } catch (e: Exception) {
            onError()
            return
        }
        onSuccess()
    }
}
