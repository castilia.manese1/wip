package com.wip.core.usecase

import com.wip.core.model.Location
import com.wip.core.repository.LocationRepository

class GetLocationsUseCase(private val repository: LocationRepository) {

    suspend operator fun invoke(onSuccess: (List<Location>) -> Unit, onError: () -> Unit) {
        val result = try {
            repository.getLocations()
        } catch (e: Exception) {
            try {
                repository.getLocationsFromDb()
            } catch (ee: Throwable) {
                onError()
                return
            }
        }
        onSuccess(result)
    }
}
