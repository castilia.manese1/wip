package com.wip.core.service

import com.wip.core.model.LocationResponse
import retrofit2.http.GET

interface LocationService {

    @GET("mylocations")
    suspend fun getLocations(): LocationResponse
}