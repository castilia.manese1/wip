package com.wip.core.db

import com.wip.core.repository.LocationPersistentLocalStorage

interface PersistentLocalSourceProvider {

    val locationPersistentLocalSourceProvider: LocationPersistentLocalStorage
}