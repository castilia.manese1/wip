package com.wip.core.repository

import com.wip.core.model.Location
import com.wip.core.service.LocationService
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.util.UUID

class LocationRemoteSource(private val service: LocationService) {

    suspend fun getLocations() = withContext(Dispatchers.IO) {
        val locationResponse = service.getLocations()
        return@withContext locationResponse.locations.map {
            Location(
                UUID.randomUUID().toString(),
                it.lat ?: it.latitude,
                it.lng ?: it.longitude,
                it.label,
                it.address,
                it.image
            )
        }
    }
}
