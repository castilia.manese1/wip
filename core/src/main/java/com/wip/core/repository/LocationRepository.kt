package com.wip.core.repository

import com.wip.core.model.Location
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class LocationRepository(private val localSource: LocationPersistentLocalStorage, private val remoteSource: LocationRemoteSource) {

    suspend fun getLocations() = withContext(Dispatchers.IO) {
        return@withContext remoteSource.getLocations()
    }

    suspend fun getLocationsFromDb() = localSource.getAll()

    suspend fun addLocationsToDb(locations: List<Location>) = localSource.replaceAll(locations)

    suspend fun createLocation(location: Location) = localSource.createLocation(location)

    suspend fun updateLocation(location: Location) = localSource.updateLocation(location)
}
