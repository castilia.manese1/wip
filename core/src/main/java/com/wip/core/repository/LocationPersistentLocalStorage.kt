package com.wip.core.repository

import com.wip.core.model.Location

interface LocationPersistentLocalStorage {

    suspend fun getAll(): List<Location>

    suspend fun replaceAll(locations: List<Location>)

    suspend fun createLocation(location: Location)

    suspend fun updateLocation(location: Location)
}
