package com.wip.wip.database.persistent

import com.wip.core.model.Location
import com.wip.core.repository.LocationPersistentLocalStorage
import com.wip.wip.database.dao.LocationDao

class LocationLocalStorage(private val locationDao: LocationDao) : LocationPersistentLocalStorage {

    override suspend fun getAll(): List<Location> = locationDao.getAll()

    override suspend fun replaceAll(locations: List<Location>) = locationDao.replaceAll(locations)

    override suspend fun createLocation(location: Location) = locationDao.insertLocation(location)

    override suspend fun updateLocation(location: Location) = locationDao.updateLocation(location)
}