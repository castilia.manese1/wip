package com.wip.wip.database

import com.wip.core.db.PersistentLocalSourceProvider
import com.wip.core.repository.LocationPersistentLocalStorage
import com.wip.wip.database.persistent.LocationLocalStorage

internal class LocalSourceProvider(locationLocalStorage: LocationLocalStorage) : PersistentLocalSourceProvider {

    override val locationPersistentLocalSourceProvider: LocationPersistentLocalStorage = locationLocalStorage
}
