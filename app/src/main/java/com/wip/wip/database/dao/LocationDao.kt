package com.wip.wip.database.dao

import com.wip.core.model.Location
import com.wip.wip.database.LocationEntity
import io.realm.Realm
import io.realm.kotlin.where

class LocationDao(private val realm: Realm) {

    fun getAll() =
        realm.where<LocationEntity>().findAll().map {
            Location(it.id, it.lat.toDouble(), it.lng.toDouble(), it.label, it.address, it.image)
        }

    fun replaceAll(locations: List<Location>) {
        realm.executeTransaction {
            deleteAll()
            insertAll(locations.map {
                LocationEntity(it.id.toString(), it.label, it.address, it.lat.toString(), it.lng.toString(), it.image.orEmpty())
            })
        }
    }

    fun insertLocation(location: Location) {
        realm.executeTransaction {
            insert(location)
        }
    }

    fun updateLocation(location: Location) {
        realm.executeTransaction {
            update(location)
        }
    }

    private fun insert(location: Location) {
        realm.insert(location.run {
            LocationEntity(
                this.id.toString(),
                this.label,
                this.address,
                this.lat.toString(),
                this.lng.toString(),
                this.image.orEmpty()
            )
        })
    }

    private fun update(location: Location) {
        realm.insertOrUpdate(location.run {
            LocationEntity(
                this.id.toString(),
                this.label,
                this.address,
                this.lat.toString(),
                this.lng.toString(),
                this.image.orEmpty()
            )
        })
    }

    private fun deleteAll() {
        realm.deleteAll()
    }

    private fun insertAll(locations: List<LocationEntity>) {
        realm.insert(locations)
    }
}