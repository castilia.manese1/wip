package com.wip.wip.database

import io.realm.RealmObject
import io.realm.annotations.PrimaryKey

open class LocationEntity(
    @PrimaryKey
    var id: String = "",
    var label: String = "",
    var address: String = "",
    var lat: String = "",
    var lng: String = "",
    var image: String? = ""
) : RealmObject()