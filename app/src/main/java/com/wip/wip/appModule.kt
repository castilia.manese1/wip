package com.wip.wip

import com.wip.core.coreModule
import com.wip.wip.database.LocalSourceProvider
import com.wip.wip.database.dao.LocationDao
import com.wip.wip.database.persistent.LocationLocalStorage
import com.wip.wip.location.LocationListViewModel
import com.wip.wip.location.add.AddLocationViewModel
import com.wip.wip.location.details.LocationDetails
import com.wip.wip.location.details.edit.EditLocationViewModel
import com.wip.wip.utils.SharedPrefManager
import io.realm.Realm
import org.koin.android.ext.koin.androidApplication
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

private val appModule = module {
    single { SharedPrefManager(androidApplication().applicationContext) }
    single { Realm.getDefaultInstance() }

    single { LocationDao(get()) }

    factory { LocationLocalStorage(get()) }
    single { LocalSourceProvider(get()) }

    viewModel { LocationListViewModel(get(),get()) }
    viewModel { (location: LocationDetails) -> EditLocationViewModel(location,get()) }
    viewModel { AddLocationViewModel(get()) }
}

val allModules = appModule + coreModule { get<LocalSourceProvider>() }
