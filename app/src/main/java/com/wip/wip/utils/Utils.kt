package com.wip.wip.utils

import android.content.Context
import android.graphics.drawable.Drawable
import android.location.Location
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.ImageView
import androidx.core.content.ContextCompat
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.CircleCrop
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import com.wip.wip.R
import com.wip.wip.location.LocationListFragment

fun LiveData<Boolean>.observeAndUpdateSwipeRefreshLayout(lifecycleOwner: LifecycleOwner, layout: SwipeRefreshLayout) =
    observe(lifecycleOwner, Observer {
        layout.isRefreshing = it
    })

fun computeDistance(myLocation: Location, stopLang: Double, stopLong: Double): Float {
    val locationB = Location(LocationListFragment.LOCATION_B).apply {
        latitude = stopLang
        longitude = stopLong
    }
    return myLocation.distanceTo(locationB).div(1000)
}

fun ImageView.loadWithTransition(context: Context, url: String, transitionName: String, startTransition: () -> Unit) {
    this.transitionName = transitionName
    val placeHolder = ContextCompat.getDrawable(context, R.drawable.img_location)
    val request = Glide.with(context).load(url)
        .placeholder(placeHolder)
        .fallback(placeHolder)
        .error(placeHolder)
        .transform(CircleCrop())
        .listener(object : RequestListener<Drawable> {
            override fun onLoadFailed(p0: GlideException?, p1: Any?, p2: Target<Drawable>?, p3: Boolean): Boolean {
                startTransition()
                return false
            }

            override fun onResourceReady(p0: Drawable?, p1: Any?, p2: Target<Drawable>?, p3: DataSource?, p4: Boolean): Boolean {
                setImageDrawable(p0)
                startTransition()
                return true
            }
        })
    request.transform(CenterCrop(), RoundedCorners(context.resources.getInteger(R.integer.image_rounded_corners)))
        .into(this)
}

fun View.hideKeyboard() {
    val inputMethodManager = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager?
    inputMethodManager?.hideSoftInputFromWindow(windowToken, 0)
}