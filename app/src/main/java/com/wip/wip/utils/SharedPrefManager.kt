package com.wip.wip.utils

import android.content.Context
import android.preference.PreferenceManager
import kotlin.properties.ReadWriteProperty
import kotlin.reflect.KProperty

class SharedPrefManager(context: Context) {

    private val preferences = PreferenceManager.getDefaultSharedPreferences(context)
    var myLatitude by PreferenceFieldDelegate.Float(MY_LATITUDE)
    var myLongitude by PreferenceFieldDelegate.Float(MY_LONGITUDE)

    private sealed class PreferenceFieldDelegate<T>(protected val key: String) : ReadWriteProperty<SharedPrefManager, T> {

        class Float(key: String) : PreferenceFieldDelegate<kotlin.Float>(key) {

            override fun getValue(thisRef: SharedPrefManager, property: KProperty<*>) = thisRef.preferences.getFloat(key, 0.0F)

            override fun setValue(thisRef: SharedPrefManager, property: KProperty<*>, value: kotlin.Float) = thisRef.preferences.edit().putFloat(key, value).apply()
        }
    }

    companion object {
        private const val MY_LATITUDE = "MY_LATITUDE"
        private const val MY_LONGITUDE = "MY_LONGITUDE"
    }
}
