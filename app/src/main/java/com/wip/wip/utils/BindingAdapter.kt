package com.wip.wip.utils

import android.graphics.drawable.Drawable
import android.view.View
import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.CircleCrop
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.wip.wip.R

@BindingAdapter("isVisible")
fun View.setVisibility(isVisible: Boolean) {
    visibility = if (isVisible) View.VISIBLE else View.GONE
}

@BindingAdapter("imageRounded", "placeholder", requireAll = false)
fun ImageView.loadImageRounded(url: String?, placeHolder: Drawable?) {
    var request = Glide.with(context).load(url)
    request = if (placeHolder != null) {
        request.placeholder(placeHolder)
            .fallback(placeHolder)
            .error(placeHolder)
            .transform(CircleCrop())
    } else {
        request.placeholder(R.color.primary)
            .fallback(R.color.primary)
            .error(R.color.primary)
            .transform(CircleCrop())
    }
    request.transform(CenterCrop(), RoundedCorners(context.resources.getInteger(R.integer.image_rounded_corners)))
        .into(this)
}