package com.wip.wip.location.details

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.transition.TransitionInflater
import com.halcyonmobile.android.common.extensions.navigation.findSafeNavController
import com.wip.wip.LocationDetailsFragmentBinding
import com.wip.wip.R
import com.wip.wip.utils.loadWithTransition

class LocationDetailsFragment : Fragment(R.layout.fragment_location_details) {

    private val args: LocationDetailsFragmentArgs by navArgs()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        sharedElementEnterTransition = TransitionInflater.from(requireContext()).inflateTransition(android.R.transition.move)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val binding = LocationDetailsFragmentBinding.bind(view)
        binding.lifecycleOwner = viewLifecycleOwner
        binding.location = args.location
        binding.fragment = this

        postponeEnterTransition()
        binding.image.loadWithTransition(requireContext(), args.location.image.orEmpty(), args.location.id) { startPostponedEnterTransition() }

        binding.navigationIcon.setOnClickListener { findNavController().navigateUp() }
        binding.edit.setOnClickListener {
            findSafeNavController().navigate(
                LocationDetailsFragmentDirections.actionLocationDetailsFragmentToEditLocationFragment(
                    args.location
                )
            )
        }
    }
}
