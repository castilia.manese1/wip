package com.wip.wip.location

import android.Manifest
import android.annotation.SuppressLint
import android.content.pm.PackageManager
import android.location.Location
import android.os.Bundle
import android.view.View
import androidx.core.app.ActivityCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.fragment.FragmentNavigatorExtras
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.gms.location.LocationServices
import com.halcyonmobile.android.common.extensions.navigation.findSafeNavController
import com.wip.wip.LocationListFragmentBinding
import com.wip.wip.R
import com.wip.wip.location.details.LocationDetails
import com.wip.wip.utils.SharedPrefManager
import com.wip.wip.utils.observeAndUpdateSwipeRefreshLayout
import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.ext.android.viewModel

class LocationListFragment : Fragment(R.layout.fragment_location_list) {

    private val viewModel: LocationListViewModel by viewModel()
    private val sharedPrefManager: SharedPrefManager by inject()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val binding = LocationListFragmentBinding.bind(view)
        binding.lifecycleOwner = viewLifecycleOwner
        binding.viewModel = viewModel

        viewModel.isLoading.observeAndUpdateSwipeRefreshLayout(viewLifecycleOwner, binding.swipeRefresh)
        setupRecyclerView(binding)
        binding.add.setOnClickListener { findSafeNavController().navigate(LocationListFragmentDirections.actionLocationListFragmentToAddLocationFragment()) }
    }

    private fun setupRecyclerView(binding: LocationListFragmentBinding) {
        val locationAdapter = LocationAdapter(
            myLocation = Location(LOCATION_A).apply {
                latitude = sharedPrefManager.myLatitude.toDouble()
                longitude = sharedPrefManager.myLongitude.toDouble()
            },
            onLocationClicked = { location, image ->
                val extras = FragmentNavigatorExtras(image to location.id)
                findSafeNavController().navigate(
                    LocationListFragmentDirections.actionLocationListFragmentToLocationDetailsFragment(
                        location = LocationDetails(location.id, location.lat, location.lng, location.label, location.address, location.image)
                    ), extras
                )
            })

        binding.recyclerView.apply {
            layoutManager = LinearLayoutManager(requireContext())
            adapter = locationAdapter
        }

        viewModel.locationList.observe(viewLifecycleOwner, Observer {
            if (checkForPermissions()) {
                requestPermissions(arrayOf(Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION), REQUEST_LOCATION)
            } else {
                getLastLocationNewMethod()
            }
            if (it.isNotEmpty()) locationAdapter.submitList(it)
        })
    }

    @SuppressLint("MissingPermission")
    private fun getLastLocationNewMethod() {
        val mFusedLocationClient = LocationServices.getFusedLocationProviderClient(requireActivity())
        if (checkForPermissions()) {
            requestPermissions(arrayOf(Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION), REQUEST_LOCATION)
        } else {
            mFusedLocationClient.lastLocation
                .addOnSuccessListener { location ->
                    location?.let {
                        sharedPrefManager.myLatitude = it.latitude.toFloat()
                        sharedPrefManager.myLongitude = it.longitude.toFloat()
                    }
                }
                .addOnFailureListener { e -> e.printStackTrace() }
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            REQUEST_LOCATION -> {
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (checkForPermissions()) {
                        getLastLocationNewMethod()
                    }
                }
                return
            }
        }
    }

    private fun checkForPermissions(): Boolean =
        (ActivityCompat.checkSelfPermission(requireContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) ||
                ActivityCompat.checkSelfPermission(requireContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED

    override fun onResume() {
        super.onResume()
        viewModel.onRetry()
    }
    companion object {
        private const val REQUEST_LOCATION = 1
        const val LOCATION_A = "LOCATION_A"
        const val LOCATION_B = "LOCATION_B"
    }
}
