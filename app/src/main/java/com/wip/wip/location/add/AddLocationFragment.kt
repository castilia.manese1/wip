package com.wip.wip.location.add

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.wip.wip.AddLocationFragmentBinding
import com.wip.wip.R
import com.wip.wip.utils.hideKeyboard
import org.koin.androidx.viewmodel.ext.android.viewModel

class AddLocationFragment : Fragment(R.layout.fragment_add_location) {

    private val viewModel: AddLocationViewModel by viewModel()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val binding = AddLocationFragmentBinding.bind(view)
        binding.lifecycleOwner = viewLifecycleOwner
        binding.viewModel = viewModel

        binding.navigationIcon.setOnClickListener { findNavController().navigateUp() }

        viewModel.isSuccessful.observe(viewLifecycleOwner, Observer {
            it?.let {
                if (it) {
                    view.hideKeyboard()
                    findNavController().navigateUp()
                }
            }
        })
    }
}
