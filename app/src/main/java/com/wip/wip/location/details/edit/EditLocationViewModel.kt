package com.wip.wip.location.details.edit

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.map
import androidx.lifecycle.viewModelScope
import com.wip.core.model.Location
import com.wip.core.usecase.UpdateLocationUseCase
import com.wip.wip.location.details.LocationDetails
import kotlinx.coroutines.launch

class EditLocationViewModel(
    private val location: LocationDetails,
    private val updateLocation: UpdateLocationUseCase
) : ViewModel() {

    val label = MutableLiveData<String>().apply { value = location.label }
    val address = MutableLiveData<String>().apply { value = location.address }
    val latitude = MutableLiveData<String>().apply { value = location.lat.toString() }
    val longitude = MutableLiveData<String>().apply { value = location.lng.toString() }
    val image = MutableLiveData<String>().apply { value = location.image }

    private val _state = MutableLiveData<State>().apply { value = State.NORMAL }
    val isSuccessful: LiveData<Boolean> = _state.map { it == State.SUCCESS }
    val isError: LiveData<Boolean> = _state.map { it == State.ERROR }
    val isLoading: LiveData<Boolean> = _state.map { it == State.LOADING }
    val isNormal: LiveData<Boolean> = _state.map { it == State.NORMAL }

    val buttonIsEnabled: LiveData<Boolean> = MediatorLiveData<Boolean>().apply {
        addSource(label) { value = checkNonNullOrEmpty(it) }
        addSource(address) { value = checkNonNullOrEmpty(it) }
        addSource(latitude) { value = checkNonNullOrEmpty(it) }
        addSource(longitude) { value = checkNonNullOrEmpty(it) }
    }

    private fun checkNonNullOrEmpty(
        labelValue: String? = label.value,
        addressValue: String? = address.value,
        latitudeValue: String? = latitude.value,
        longitudeValue: String? = longitude.value
    ) = !labelValue.isNullOrEmpty() && !addressValue.isNullOrEmpty() && latitudeValue != null && longitudeValue != null

    fun updateLocations() {
        _state.value = State.LOADING
        viewModelScope.launch {
            updateLocation(
                location = Location(
                    location.id,
                    latitude.value?.toDouble(),
                    longitude.value?.toDouble(),
                    label.value.toString(),
                    address.value.toString(),
                    image.value.orEmpty()
                ),
                onSuccess = {
                    _state.value = State.SUCCESS
                },
                onError = {
                    _state.value = State.ERROR
                }
            )
        }
    }

    enum class State {
        LOADING,
        NORMAL,
        ERROR,
        SUCCESS
    }
}
