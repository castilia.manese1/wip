package com.wip.wip.location

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.map
import androidx.lifecycle.viewModelScope
import com.wip.core.model.Location
import com.wip.core.usecase.AddLocationsUseCase
import com.wip.core.usecase.GetLocationsUseCase
import com.wip.wip.location.details.LocationDetails
import kotlinx.coroutines.launch
import java.util.UUID

class LocationListViewModel(
    private val getLocations: GetLocationsUseCase,
    private val addLocations: AddLocationsUseCase
) : ViewModel() {

    private val _locationList = MutableLiveData<List<LocationDetails>>()
    val locationList: LiveData<List<LocationDetails>> = _locationList

    private val _state = MutableLiveData<State>()
    val isNormal: LiveData<Boolean> = _state.map { it == State.NORMAL }
    val isError: LiveData<Boolean> = _state.map { it == State.ERROR }
    val isLoading: LiveData<Boolean> = _state.map { it == State.LOADING }
    val isInitialLoading: LiveData<Boolean> = _state.map { it == State.INITIAL_LOADING }

    init {
        _state.value = State.INITIAL_LOADING
        getInitialData()
    }

    private fun getInitialData() {
        viewModelScope.launch {
            getLocations(
                onSuccess = {
                    _locationList.value = it.map { location ->
                        LocationDetails(location.id.orEmpty(), location.lat, location.lng, location.label, location.address, location.image)
                    }
                    addAllLocations(it)
                    _state.value = State.NORMAL
                }, onError = {
                    _state.value = State.ERROR
                })
        }
    }

    private fun addAllLocations(locations: List<Location>) {
        viewModelScope.launch {
            addLocations(
                locations = locations,
                onSuccess = {
                    _state.value = State.SUCCESS
                },
                onError = {
                    _state.value = State.ADD_ERROR
                }
            )
        }
    }

    fun onRetry() {
        _state.value = State.LOADING
        _locationList.value = emptyList()
        getInitialData()
    }

    enum class State {
        INITIAL_LOADING,
        LOADING,
        ERROR,
        ADD_ERROR,
        NORMAL,
        SUCCESS
    }
}
