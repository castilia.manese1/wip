package com.wip.wip.location.details

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class LocationDetails(
    val id : String,
    val lat: Double?,
    val lng: Double?,
    val label: String,
    val address: String,
    val image: String?
) : Parcelable
