package com.wip.wip.location.details.edit

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.wip.wip.EditLocationFragmentBinding
import com.wip.wip.R
import com.wip.wip.utils.hideKeyboard
import org.koin.androidx.viewmodel.ext.android.viewModel
import org.koin.core.parameter.parametersOf

class EditLocationFragment : Fragment(R.layout.fragment_edit_location) {

    private val args: EditLocationFragmentArgs by navArgs()
    private val viewModel: EditLocationViewModel by viewModel { parametersOf(args.location) }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val binding = EditLocationFragmentBinding.bind(view)
        binding.lifecycleOwner = viewLifecycleOwner
        binding.viewModel = viewModel

        binding.navigationIcon.setOnClickListener { findNavController().navigateUp() }

        viewModel.isSuccessful.observe(viewLifecycleOwner, Observer {
            it?.let {
                if (it) {
                    view.hideKeyboard()
                    findNavController().navigateUp()
                }
            }
        })
    }
}
