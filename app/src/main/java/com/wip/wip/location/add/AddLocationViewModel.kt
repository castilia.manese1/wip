package com.wip.wip.location.add

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.map
import androidx.lifecycle.viewModelScope
import com.wip.core.model.Location
import com.wip.core.usecase.CreateLocationUseCase
import kotlinx.coroutines.launch
import java.util.UUID

class AddLocationViewModel(private val createLocation: CreateLocationUseCase) : ViewModel() {

    val label = MutableLiveData<String>()
    val address = MutableLiveData<String>()
    val latitude = MutableLiveData<String>()
    val longitude = MutableLiveData<String>()
    val image = MutableLiveData<String>()

    private val _state = MutableLiveData<State>().apply { value = State.NORMAL }
    val isSuccessful: LiveData<Boolean> = _state.map { it == State.SUCCESS }
    val isError: LiveData<Boolean> = _state.map { it == State.ERROR }
    val isLoading: LiveData<Boolean> = _state.map { it == State.LOADING }
    val isNormal: LiveData<Boolean> = _state.map { it == State.NORMAL }

    val buttonIsEnabled: LiveData<Boolean> = MediatorLiveData<Boolean>().apply {
        addSource(label) { value = checkNonNullOrEmpty(labelValue = it) }
        addSource(address) { value = checkNonNullOrEmpty(addressValue = it) }
        addSource(latitude) { value = checkNonNullOrEmpty(latitudeValue = it) }
        addSource(longitude) { value = checkNonNullOrEmpty(longitudeValue = it) }
    }

    private fun checkNonNullOrEmpty(
        labelValue: String? = label.value,
        addressValue: String? = address.value,
        latitudeValue: String? = latitude.value,
        longitudeValue: String? = longitude.value
    ) = !labelValue.isNullOrEmpty() && !addressValue.isNullOrEmpty() && !latitudeValue.isNullOrEmpty() && !longitudeValue.isNullOrEmpty()

    fun createLocation() {
        _state.value = State.LOADING
        viewModelScope.launch {
            createLocation(
                location = Location(
                    UUID.randomUUID().toString(),
                    latitude.value?.toDouble(),
                    longitude.value?.toDouble(),
                    label.value.toString(),
                    address.value.toString(),
                    image.value.orEmpty()
                ),
                onSuccess = {
                    _state.value = State.SUCCESS
                },
                onError = {
                    _state.value = State.ERROR
                }
            )
        }
    }

    enum class State {
        LOADING,
        NORMAL,
        ERROR,
        SUCCESS
    }
}
