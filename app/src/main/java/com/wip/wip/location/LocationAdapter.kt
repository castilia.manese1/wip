package com.wip.wip.location

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.wip.wip.ItemLocationViewBinding
import com.wip.wip.R
import com.wip.wip.location.details.LocationDetails
import com.wip.wip.utils.computeDistance

class LocationAdapter(private val myLocation: android.location.Location, private val onLocationClicked: (LocationDetails, ImageView) -> Unit) :
    ListAdapter<LocationDetails, LocationAdapter.LocationViewHolder>(LocationDiffUtilCallback()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): LocationViewHolder =
        LocationViewHolder(DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.view_item_location, parent, false))

    override fun onBindViewHolder(holder: LocationViewHolder, position: Int) = holder.bind(myLocation, getItem(position), onLocationClicked)

    class LocationViewHolder(private val binding: ItemLocationViewBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(myLocation: android.location.Location, location: LocationDetails, onItemClicked: (LocationDetails, ImageView) -> Unit) {
            binding.location = location
            binding.myDistance = computeDistance(myLocation, location.lat ?: 0.0, location.lng ?: 0.0)

            binding.root.setOnClickListener { onItemClicked(location, binding.image) }
        }
    }

    class LocationDiffUtilCallback : DiffUtil.ItemCallback<LocationDetails>() {
        override fun areItemsTheSame(oldItem: LocationDetails, newItem: LocationDetails): Boolean = oldItem.id == newItem.id

        override fun areContentsTheSame(oldItem: LocationDetails, newItem: LocationDetails): Boolean = oldItem == newItem
    }
}
